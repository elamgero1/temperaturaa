#!/bin/bash
C=$1
T=$2

echo '********************************************'
echo 'Programa de Calculo de Temperatura'
echo '********************************************'
echo 'Grados Centígrados: '$C

if [ $T == 'F' ]
then
        echo 'Tipo de Conversion: Farenheit'
elif [ $T == 'K' ]
then
	echo 'Tipo de Conversion: Kelvin'
fi

if [ $T == 'F' ]
then
	F=$(echo "$1 * 1.8 + 32" | bc -l )
	echo 'Resultado: '$F
elif [ $T == 'K' ]
then
        F=$(echo "$1 + 273.15" | bc -l )
        echo 'Resultado: '$F
else 
	echo 'No existe parametro ingresado'
fi

